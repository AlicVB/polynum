# ![alt text](https://framagit.org/AlicVB/polynum/raw/master/polynum2.png "polynum")

rituel de calcul mental

## exemple
![alt text](https://framagit.org/AlicVB/polynum/raw/master/capture.png "exemple polynum")

## détails
polynum est une application web pour créer rapidement des polynômes de calcul mental.
Les polynômes et les calculs sont générés au hasard en tenant des critères initiaux.

3 type de supports de sortie sont proposés :
- impression (via fichier pdf)
- vidéoprojecteur
- jeu en ligne

## crédits
- idée d'Adrien Ferreira De Souza, IEN
- icônes issues du theme libre "gartoonredux" (https://www.gnome-look.org/p/1012491)

## licence
- Ce logiciel est sous licence GPL v3
