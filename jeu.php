<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) 2020 A.RENAUDIN  Developer

    This file is part of exoTICE.

    exoTICE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    exoTICE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with exoTICE.  If not, see <https://www.gnu.org/licenses/>.
*/

include("utils.php");

// on récupère les valeurs passées en paramètres
$ok = isset($_GET['nb_sommets']);

$_nbs = 3; // nomùbre de sommets
if (isset($_GET['nb_sommets']) && is_numeric($_GET['nb_sommets'])) $_nbs = max(3, min(5, intval($_GET['nb_sommets'])));
$_vmin = 0; // valeur mini des sommets
if (isset($_GET['val_min']) && is_numeric($_GET['val_min'])) $_vmin = intval($_GET['val_min']);
$_vmax = 9; // valeur maxi des sommets
if (isset($_GET['val_max']) && is_numeric($_GET['val_max'])) $_vmax = intval($_GET['val_max']);
//les opérations
$_op = array();
if (isset($_GET['op_plus'])) $_op[] = "+";
if (isset($_GET['op_moins'])) $_op[] = "-";
if (isset($_GET['op_fois'])) $_op[] = "x";
if (isset($_GET['op_div'])) $_op[] = ":";
if (count($_op) == 0) $_op = ["+", "-"];

$_nbe = 3; // nombre d'étapes des questions
if (isset($_GET['nb_etapes']) && is_numeric($_GET['nb_etapes'])) $_nbe = max(2, min(20, intval($_GET['nb_etapes'])));
$_cmin = 0; // valeur mini des calculs
if (isset($_GET['calc_min']) && is_numeric($_GET['calc_min'])) $_cmin = intval($_GET['calc_min']);
$_cmax = 19; // valeur maxi des calculs
if (isset($_GET['calc_max']) && is_numeric($_GET['calc_max'])) $_cmax = intval($_GET['calc_max']);
$_nbt = 0; // nombre de trous des calculs
if (isset($_GET['nb_trous']) && is_numeric($_GET['nb_trous'])) $_nbt = max(0, min($_nbe, intval($_GET['nb_trous'])));

$_nbq = 9; // nombre de questions
if (isset($_GET['nb_questions']) && is_numeric($_GET['nb_questions'])) $_nbq = max(1, min(40, intval($_GET['nb_questions'])));
$_nbeach = 3; // changement de polynome
if (isset($_GET['nb_change']) && is_numeric($_GET['nb_change'])) $_nbeach = max(1, min(10, intval($_GET['nb_change'])));

// on fait les calculs de départ
$nb_exo = (int)$_nbq/$_nbeach;
if ($_nbq%$_nbeach > 0) $nb_exo++;
$c = 0;
$tvals = [];
$tques = [];
while($c<20*$nb_exo && count($tvals)<$nb_exo)
{
  $vals = valeurs($_nbs, $_vmin, $_vmax, $_op);
  // on vérifie qu'on a pas déjà ces valeurs
  $deja = false;
  for ($i=0; $i<count($tvals); $i++)
  {
    if ($tvals[$i] === $vals)
    {
      $deja = true;
      break;
    }
  }
  if (!$deja)
  {
    $ques = questions($vals['valeurs'], $vals['operations'], $_nbe, $_cmin, $_cmax);
    if (count($ques)>= $_nbeach)
    {
      $tvals[] = $vals;
      shuffle($ques);
      for ($i=0; $i<$_nbeach; $i++) $tques[] = $ques[$i];
    }
  }

  $c++;
}

// attention, on limite le nombre de questions à juste ce qu'on veut
$tques = array_slice($tques, 0, $_nbq);

//les traductions
$lang = "fr";
if ($_GET['lang'] == "en") $lang = "en";

if ($lang == "fr")
{
  $trad[] = "polynum -- jeu en ligne";
  $trad[] = "© A. RENAUDIN 2020 -- impulsé par A. FERREIRA DE SOUZA";
}
else if ($lang == "en")
{
  $trad[] = "polynum -- online game";
  $trad[] = "© A. RENAUDIN 2020 -- impulse by A. FERREIRA DE SOUZA";
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta name="mobile-web-app-capable" content="yes">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title><?php echo $trad[0]; ?></title>
  <link rel="shortcut icon" href="polynum.png" >
  <link rel="stylesheet" href="jeu2.css">
  <script type="text/javascript" src="jeu.js"></script>
</head>

<body id="video_body" onload="depart('<?php echo $_nbeach; ?>', '<?php echo $lang; ?>')">
  <div id="video_main">
    <?php
      // on affiche le polynome
      echo "<div>";
      foreach($tvals as $vals)
      {
        echo polynum($vals['valeurs'], $vals['operations'], ($_nbt>0));
      }
      echo "</div>";

      // on affiche l'ensemble des questions
      echo "<div id='page2'>";
      echo affiche_question_jeu($tques, $_nbt, $tvals[0]['valeurs']);
      echo "</div>";
    ?>
  </div>
  <div id="credits">
      <a href="index.php?lang=<?php echo $lang; ?>"><img src="polynum2inv.svg"/></a>
      <a href='../contact.php'><img id='contact' src='mail.svg'/><?php echo $trad[1]; ?><img id='contact' src='mail.svg'/></a>
  </div>
  <a id='retour' href='creation.php?dest=jeu&lang=<?php echo $lang; ?>'><img src='edit-undo.svg'/></a>
</body>
</html>