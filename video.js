let pos = 0;
let lang = "fr";

function reload()
{
  window.location.reload();
}

function suivant()
{
  let els = document.getElementsByClassName("calcul");
  els[pos].style.display = "none";
  pos++;
  if (pos>=els.length) pos=0;
  els[pos].style.display = "block";
}

function depart(_lang)
{
  lang = _lang
  let els = document.getElementsByClassName("calcul");
  els[0].style.display = "block";
}