<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) 2020 A.RENAUDIN  Developer

    This file is part of exoTICE.

    exoTICE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    exoTICE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with exoTICE.  If not, see <https://www.gnu.org/licenses/>.
*/

include("utils.php");
include_once("TCPDF/tcpdf.php");

function c($v)
{
  global $hexo;
  return $v*$hexo/400;
}
function coul($num)
{
  if ($num == -1) return array(146, 146, 146);
  if ($num == 0) return array(125, 196, 209);
  if ($num == 1) return array(255, 159, 75);
  if ($num == 2) return array(126, 223, 122);
  if ($num == 3) return array(249, 225, 88);
  if ($num == 4) return array(255, 71, 226);
  return array();
}
function forme($pdf, $x, $y, $size, $num, $corr=false)
{
  $fill = array(220,220,220);
  if ($corr) $fill = array(100,100,100);
  $s_line = array('width' => 0.8, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(100, 100, 100));
  if ($num == 0) $pdf->RegularPolygon($x, $y, $size*1.4, 4, 45, false, 'DF', array('all' => $s_line), $fill);
  else if ($num == 1) $pdf->Circle($x, $y, $size, 0, 360, 'DF', $s_line, $fill);
  else if ($num == 2) $pdf->RegularPolygon($x, $y, $size*1.4, 3, 60, false, 'DF', array('all' => $s_line), $fill);
  else if ($num == 3) $pdf->RegularPolygon($x, $y, $size*1.25, 4, 0, false, 'DF', array('all' => $s_line), $fill);
  else if ($num == 4) $pdf->RegularPolygon($x, $y, $size*1.2, 6, 0, false, 'DF', array('all' => $s_line), $fill);
}

function polynum2($pdf, $valeurs, $operations, $coul)
{
  global $mx;
  global $my;

  $rad = c(36);
  $nbs = count($valeurs);

  $s_line = array('width' => 1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));

  if ($nbs == 3)
  {
    $myy = $my + c(27); // pour centrer verticalement dans un carré de 400x400
    $posx = c(400)-$rad+$mx;
    $posy = c(373)-$rad+$myy;
    // les lignes
    $pdf->Line(c(200) + $mx, $rad + $myy, $rad + $mx, $posy, $s_line);
    $pdf->Line(c(200) + $mx, $rad + $myy, $posx, $posy, $s_line);
    $pdf->Line($rad + $mx, $posy, $posx, $posy, $s_line);
    //les cercles des sommets
    if ($coul)
    {
      $pdf->Image("ball0.png", $mx + c(200) - $rad, $myy, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
      $pdf->Image("ball1.png", $mx, $posy - $rad, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
      $pdf->Image("ball2.png", $posx - $rad, $posy - $rad, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    }
    else
    {
      forme($pdf, $mx + c(200), $myy + $rad, $rad*0.9, 0);
      forme($pdf, $mx + $rad, $posy, $rad*0.9, 1);
      forme($pdf, $posx, $posy, $rad*0.9, 2);
    }

    //les chiffres dans les cercles
    $pdf->SetFont('helvetica', 'B', $rad*3);
    $pdf->SetXY(c(200) + $mx - $rad, $myy);
    $pdf->Cell($rad*2, $rad*2, $valeurs[0], 0, 0, 'C');
    $pdf->SetXY($mx, $posy - $rad);
    $pdf->Cell($rad*2, $rad*2, $valeurs[1], 0, 0, 'C');
    $pdf->SetXY($posx-$rad, $posy-$rad);
    $pdf->Cell($rad*2, $rad*2, $valeurs[2], 0, 0, 'C');
    // les signes opératoires
    $pdf->Image(op_src_png($operations[0][1]), $mx + c(100) + $rad/4, $myy - c(27) + c(200) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[1][2]), $mx + c(200) - $rad/2, $myy - c(27) + c(400) - $rad*1.5, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[2][0]), $mx + c(300) - $rad*1.25, $myy - c(27) + c(200) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
  }
  else if ($nbs == 4)
  {
    $posx = c(400)-$rad+$mx;
    $posy = c(400)-$rad+$my;
    // les lignes
    $pdf->Line($rad + $mx, $rad + $my, $rad + $mx, $posy, $s_line);
    $pdf->Line($rad + $mx, $rad + $my, $posx, $posy, $s_line);
    $pdf->Line($rad + $mx, $rad + $my, $posx, $rad + $my, $s_line);
    $pdf->Line($posx, $rad + $my, $rad + $mx, $posy, $s_line);
    $pdf->Line($posx, $posy, $rad + $mx, $posy, $s_line);
    $pdf->Line($posx, $posy, $posx, $rad + $my, $s_line);
    //les cercles des sommets
    if ($coul)
    {
      $pdf->Image("ball0.png", $mx, $my, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
      $pdf->Image("ball1.png", $posx - $rad, $my, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
      $pdf->Image("ball2.png", $posx - $rad, $posy - $rad, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
      $pdf->Image("ball3.png", $mx, $posy-$rad, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    }
    else
    {
      forme($pdf, $mx + $rad, $my + $rad, $rad*0.9, 0);
      forme($pdf, $posx, $my + $rad, $rad*0.9, 1);
      forme($pdf, $posx, $posy, $rad*0.9, 2);
      forme($pdf, $mx + $rad, $posy, $rad*0.9, 3);
    }

    //les chiffres dans les cercles
    $pdf->SetFont('helvetica', 'B', $rad*3);
    $pdf->SetXY($mx, $my);
    $pdf->Cell($rad*2, $rad*2, $valeurs[0], 0, 0, 'C');
    $pdf->SetXY($posx-$rad, $my);
    $pdf->Cell($rad*2, $rad*2, $valeurs[1], 0, 0, 'C');
    $pdf->SetXY($posx-$rad, $posy-$rad);
    $pdf->Cell($rad*2, $rad*2, $valeurs[2], 0, 0, 'C');
    $pdf->SetXY($mx, $posy-$rad);
    $pdf->Cell($rad*2, $rad*2, $valeurs[3], 0, 0, 'C');
    // les signes opératoires
    $pdf->Image(op_src_png($operations[0][1]), $mx + c(200) - $rad/2, $my + $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[1][2]), $mx + c(400) - $rad*1.5, $my + c(200) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[2][3]), $mx + c(200) - $rad/2, $my + c(400) - $rad*1.5, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[3][0]), $mx + $rad/2, $my + c(200) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[0][2]), $mx + c(133) - $rad/2, $my + c(133) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[1][3]), $mx + c(266) - $rad/2, $my + c(133) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
  }
  else if ($nbs == 5)
  {
    $posx = c(400)-$rad+$mx;
    $posy = c(400)-$rad+$my;
    // les lignes
    $pdf->Line($rad + $mx, $rad + $my, $rad + $mx, $posy, $s_line);
    $pdf->Line($rad + $mx, $rad + $my, $posx, $posy, $s_line);
    $pdf->Line($rad + $mx, $rad + $my, $posx, $rad + $my, $s_line);
    $pdf->Line($posx, $rad + $my, $rad + $mx, $posy, $s_line);
    $pdf->Line($posx, $posy, $rad + $mx, $posy, $s_line);
    $pdf->Line($posx, $posy, $posx, $rad + $my, $s_line);
    //les cercles des sommets
    if ($coul)
    {
      $pdf->Image("ball0.png", $mx, $my, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
      $pdf->Image("ball1.png", $posx - $rad, $my, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
      $pdf->Image("ball2.png", $posx - $rad, $posy - $rad, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
      $pdf->Image("ball3.png", $mx, $posy-$rad, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
      $pdf->Image("ball4.png", $mx + c(200) - $rad, $my + c(200) - $rad, $rad*2, $rad*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    }
    else
    {
      forme($pdf, $mx + $rad, $my + $rad, $rad*0.9, 0);
      forme($pdf, $posx, $my + $rad, $rad*0.9, 1);
      forme($pdf, $posx, $posy, $rad*0.9, 2);
      forme($pdf, $mx + $rad, $posy, $rad*0.9, 3);
      forme($pdf, $mx + c(200), $my + c(200), $rad*0.9, 4);
    }

    //les chiffres dans les cercles
    $pdf->SetFont('helvetica', 'B', $rad*3);
    $pdf->SetXY($mx, $my);
    $pdf->Cell($rad*2, $rad*2, $valeurs[0], 0, 0, 'C');
    $pdf->SetXY($posx-$rad, $my);
    $pdf->Cell($rad*2, $rad*2, $valeurs[1], 0, 0, 'C');
    $pdf->SetXY($posx-$rad, $posy-$rad);
    $pdf->Cell($rad*2, $rad*2, $valeurs[2], 0, 0, 'C');
    $pdf->SetXY($mx, $posy-$rad);
    $pdf->Cell($rad*2, $rad*2, $valeurs[3], 0, 0, 'C');
    $pdf->SetXY($mx + c(200) - $rad, $my + c(200)-$rad);
    $pdf->Cell($rad*2, $rad*2, $valeurs[4], 0, 0, 'C');
    // les signes opératoires
    $pdf->Image(op_src_png($operations[0][1]), $mx + c(200) - $rad/2, $my + $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[1][2]), $mx + c(400) - $rad*1.5, $my + c(200) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[2][3]), $mx + c(200) - $rad/2, $my + c(400) - $rad*1.5, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[3][0]), $mx + $rad/2, $my + c(200) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[0][4]), $mx + c(133) - $rad/2, $my + c(133) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[1][4]), $mx + c(266) - $rad/2, $my + c(133) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[4][2]), $mx + c(266) - $rad/2, $my + c(266) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
    $pdf->Image(op_src_png($operations[4][3]), $mx + c(133) - $rad/2, $my + c(266) - $rad/2, $rad, $rad, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
  }
}

function affiche_questions2($pdf, $questions, $nombre, $nb_trous, $corr, $coul)
{
  global $mx;
  global $my;

  $mxx = $mx + c(400) + 10;

  $rad = c(36);
  // on choisit les question au hasard parmis la liste
  $calcs = [];
  if (count($questions)<=$nombre) $calcs = $questions;
  else
  {
    for ($i=0; $i<$nombre; $i++)
    {
      $pos = random_int(0, count($questions)-1);
      $calcs[] = $questions[$pos];
      array_splice($questions, $pos, 1);
    }
  }

  if (count($calcs) == 0)
  {
    $pdf->SetXY($mx, $my + c(200));
    $pdf->Cell($rad*2, $rad*2, "Aucun calcul trouvé avec les conditions posées ".$nombre." ".count($questions), 0, 0, 'L');
    return;
  }

  $nbe = count($calcs[0])-1;
  // on regrade le rayon max pour que ça rentre en hauteur
  $r = min($rad, 0.7*c(200)/count($calcs));
  // et le rayon max pour que ça rentre en largeur
  $r = min($r, (200-$mxx)/(2.5*($nbe+1)));
  $space = max(0, (c(400) - count($calcs)*$r*2)/(count($calcs)+1));
  $h = ($r*2 + $space)*count($calcs);
  $nb_trous = min($nb_trous, $nbe);

  $s_line = array('width' => 1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));

  $y = $my + $r + $space;
  foreach ($calcs as $ex)
  {
    // la ligne
    $x = $mxx+2.5*$r*$nbe;
    $pdf->Line($r + $mxx, $y, $x, $y, $s_line);
    $pdf->Line($x + 0.25*$r, $y, $x - 0.25*$r, $y - 0.25*$r, $s_line);
    $pdf->Line($x + 0.25*$r, $y, $x - 0.25*$r, $y + 0.25*$r, $s_line);

    // les cercles avec leur chiffres
    $trous = [];
    while (count($trous) < $nb_trous)
    {
      $t = random_int(0, $nbe-1);
      if (array_search($t, $trous)===false) $trous[] = $t;
    }
    for ($i=0; $i<$nbe; $i++)
    {
      $x = $mxx + $r+2.5*$r*$i;
      if (array_search($i, $trous)!==false)
      {
        if ($corr)
        {
          if ($coul)
          {
            $pdf->Image("ball".$ex[$i].".png", $x - $r, $y - $r, $r*2, $r*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
            $pdf->Circle($x, $y, $r*0.9, 0, 360, 'D', $s_line, array(255,255,255));
          }
          else if ($ex[$i] == 2) forme($pdf, $x, $y + $r*0.2, $r*0.9, $ex[$i], true);
          else forme($pdf, $x, $y, $r*0.9, $ex[$i], true);
        }
        else
        {
          if ($coul) $pdf->Circle($x, $y, $r*0.9, 0, 360, 'DF', $s_line, array(255,255,255));
          else
          {
            $pdf->Circle($x, $y, $r/2, 0, 360, 'F', $s_line, array(230, 230, 230));
            $pdf->SetXY($x - $r, $y - $r);
            $pdf->Cell($r*2, $r*2, "?", 0, 0, 'C');
          }
        }
      }
      else
      {
        if ($coul) $pdf->Image("ball".$ex[$i].".png", $x - $r, $y - $r, $r*2, $r*2, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
        else if ($ex[$i] == 2) forme($pdf, $x, $y + $r*0.2, $r*0.9, $ex[$i]);
        else forme($pdf, $x, $y, $r*0.9, $ex[$i]);
      }
    }

    // et le cercle de réponse
    $x = $mxx + 1.5*$r+2.5*$r*$nbe;
    if (count($trous)>0 || $corr)
    {
      $pdf->Circle($x, $y, $r, 0, 360, 'F', array(), coul(-1));
      $pdf->SetXY($x - $r, $y - $r);
      $pdf->Cell($r*2, $r*2, $ex[$nbe], 0, 0, 'C');
    }
    else
    {
      if ($coul) $pdf->Circle($x, $y, $r*0.9, 0, 360, '', $s_line, coul(-1));
      else $pdf->Circle($x, $y, $r, 0, 360, 'F', $s_line, coul(-1));
    }

    $y += $r + $space + $r;
  }
}

// on récupère les valeurs passées en paramètres
$ok = isset($_GET['nb_sommets']);

$_nbs = 3; // nomùbre de sommets
if (isset($_GET['nb_sommets']) && is_numeric($_GET['nb_sommets'])) $_nbs = max(3, min(5, intval($_GET['nb_sommets'])));
$_vmin = 0; // valeur mini des sommets
if (isset($_GET['val_min']) && is_numeric($_GET['val_min'])) $_vmin = intval($_GET['val_min']);
$_vmax = 9; // valeur maxi des sommets
if (isset($_GET['val_max']) && is_numeric($_GET['val_max'])) $_vmax = intval($_GET['val_max']);
//les opérations
$_op = array();
if (isset($_GET['op_plus'])) $_op[] = "+";
if (isset($_GET['op_moins'])) $_op[] = "-";
if (isset($_GET['op_fois'])) $_op[] = "x";
if (isset($_GET['op_div'])) $_op[] = ":";
if (count($_op) == 0) $_op = ["+", "-"];

$_nbc = 3; // nombre de questions
if (isset($_GET['nb_calc']) && is_numeric($_GET['nb_calc'])) $_nbc = max(1, min(20, intval($_GET['nb_calc'])));
$_nbe = 3; // nombre d'étapes des questions
if (isset($_GET['nb_etapes']) && is_numeric($_GET['nb_etapes'])) $_nbe = max(2, min(20, intval($_GET['nb_etapes'])));
$_cmin = 0; // valeur mini des calculs
if (isset($_GET['calc_min']) && is_numeric($_GET['calc_min'])) $_cmin = intval($_GET['calc_min']);
$_cmax = 19; // valeur maxi des calculs
if (isset($_GET['calc_max']) && is_numeric($_GET['calc_max'])) $_cmax = intval($_GET['calc_max']);
$_nbt = 0; // nombre de trous des calculs
if (isset($_GET['nb_trous']) && is_numeric($_GET['nb_trous'])) $_nbt = max(0, min($_nbe, intval($_GET['nb_trous'])));

$_nbp = 1; // nombre de pages
if (isset($_GET['nb_pages']) && is_numeric($_GET['nb_pages'])) $_nbp = max(1, min(3, intval($_GET['nb_pages'])));
$_corr = (isset($_GET['corr'])); // avec la correction
$_coul = true; // impression couleur
if (isset($_GET['impression']) && $_GET['impression'] == "nb") $_coul = false;

// on fait les calculs de départ
$nb_exo = 3 * $_nbp;
$c = 0;
$tvals = [];
$tques = [];
while($c<20*$nb_exo && count($tvals)<$nb_exo)
{
  $vals = valeurs($_nbs, $_vmin, $_vmax, $_op);
  // on vérifie qu'on a pas déjà ces valeurs
  $deja = false;
  for ($i=0; $i<count($tvals); $i++)
  {
    if ($tvals[$i] === $vals)
    {
      $deja = true;
      break;
    }
  }
  if (!$deja)
  {
    $ques = questions($vals['valeurs'], $vals['operations'], $_nbe, $_cmin, $_cmax);
    if (count($ques)>0)
    {
      $tvals[] = $vals;
      $tques[] = $ques;
    }
  }

  $c++;
}


// on fabrique le pdf
$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);

// set document information
$pdf->SetCreator('Aldric RENAUDIN');
$pdf->SetAuthor('Aldric RENAUDIN');
$pdf->SetTitle('Polynum');
$pdf->SetSubject('Polynum');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10, 10, 10);

// set image scale factor
$pdf->setImageScale(1.662);

// set auto page breaks
$pdf->SetAutoPageBreak(FALSE, 10);

// set font
$pdf->SetFont('helvetica', 'I', 10);

$mx = 10;
$my = 10;
$hexo = 85;
$mexo = 6;
for ($i=0; $i<count($tvals); $i++)
{
  // tous les 3 exercices, on passe à une nouvelle page
  if ($i%3 == 0)
  {
    $pdf->AddPage();
    $my = 10;
    // on fait les refs en bas de page
    $pdf->Image("polynum.png", 10, 280, 8, 8, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
    $pdf->SetXY(18, 280);
    $pdf->SetFont('helvetica', 'I', 10);
    $pdf->Cell(100, 8, 'https://exotice.fr/polynum   © A. RENAUDIN 2020', 0, 0, 'L', false, 'https://exotice.fr/polynum');
  }
  polynum2($pdf, $tvals[$i]['valeurs'], $tvals[$i]['operations'], $_coul);
  affiche_questions2($pdf, $tques[$i], $_nbc, $_nbt, false, $_coul);
  $my += $hexo + $mexo;
}

// et on fait la même chose pour la correction
if ($_corr)
{
  for ($i=0; $i<count($tvals); $i++)
  {
    // tous les 3 exercices, on passe à une nouvelle page
    if ($i%3 == 0)
    {
      $pdf->AddPage();
      $my = 10;
      // on fait les refs en bas de page
      $pdf->Image("polynum.png", 10, 280, 8, 8, 'PNG', '', '', true, 150, '', false, false, 0, false, false, false);
      $pdf->SetXY(18, 280);
      $pdf->SetFont('helvetica', 'I', 10);
      $pdf->Cell(100, 8, 'https://exotice.fr/polynum   © A. RENAUDIN 2020', 0, 0, 'L', false, 'https://exotice.fr/polynum');
    }
    polynum2($pdf, $tvals[$i]['valeurs'], $tvals[$i]['operations'], $_coul);
    affiche_questions2($pdf, $tques[$i], $_nbc, $_nbt, true, $_coul);
    $my += $hexo + $mexo;
  }
}



//Close and output PDF document
$pdf->Output('polynum_'.time().'.pdf', 'D');

?>