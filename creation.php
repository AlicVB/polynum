<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) 2020 A.RENAUDIN  Developer

    This file is part of polynum.

    exoTICE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    exoTICE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with polynum.  If not, see <https://www.gnu.org/licenses/>.
*/

// on récupère les valeurs passées en paramètres
$_nbs = 3; // nomùbre de sommets
$_vmin = 0; // valeur mini des sommets
$_vmax = 9; // valeur maxi des sommets
//les opérations
$_op = ["+", "-"];

$_nbc = 4; // nombre de questions
$_nbe = 4; // nombre d'étapes des questions
$_cmin = 0; // valeur mini des calculs
$_cmax = 19; // valeur maxi des calculs
$_nbt = 0; // nombre de trous des calculs

$_nbp = 1; // nombre de pages
$_corr = true; // avec la correction

$_nbq = 9; // nombre de questions pour le jeu
$_nbeach = 3; // chager le polynome toutes les ... questions

$dest = "pdf.php";
if ($_GET['dest'] == "jeu") $dest = "jeu.php";
else if ($_GET['dest'] == "video") $dest = "video.php";

// et on gère les langues
$lang = "fr";
if ($_GET['lang'] == "en") $lang = "en";

if ($lang == "fr")
{
  $trad[] = "polynum -- création";
  $trad[] = "rituel de calcul";
  $trad[] = "vidéoprojecteur";
  $trad[] = "impression";
  $trad[] = "jeu en ligne";
  $trad[] = "Nombre de sommets";
  $trad[] = "Valeurs des sommets";
  $trad[] = "Opérations";
  $trad[] = "Nombre de calculs";
  $trad[] = "par polynôme";
  $trad[] = "Nombre d'étapes des calculs";
  $trad[] = "Nombre de trous des calculs";
  $trad[] = "(0 = calcul normal)";
  $trad[] = "Bornes des calculs";
  $trad[] = "Mise en page";
  $trad[] = "nb de pages :";
  $trad[] = "avec la correction";
  $trad[] = "Type d'impression";
  $trad[] = "couleur";
  $trad[] = "noir et blanc";
  $trad[] = "Mode d'emploi :<br>- clic sur le polynome => chargement d'un nouveau polygone<br>- clic sur le calcul => on montre la correction<br>- nouveau clic sur le calcul => on passe au calcul suivant";
  $trad[] = "nombre de questions";
  $trad[] = "polynômes";
  $trad[] = "Changer le polynôme toutes les";
  $trad[] = "questions";
  $trad[] = "générer";
  $trad[] = "© A. RENAUDIN 2020 -- impulsé par A. FERREIRA DE SOUZA";
  $trad[] = "logiciel libre -- code source";
}
else if ($lang == "en")
{
  $trad[] = "polynum -- maker";
  $trad[] = "mental calculation";
  $trad[] = "projectors";
  $trad[] = "printers";
  $trad[] = "online game";
  $trad[] = "Number of vertices";
  $trad[] = "Vertices values";
  $trad[] = "Operations";
  $trad[] = "Number of calculations";
  $trad[] = "per polynomial";
  $trad[] = "Number of calculation steps";
  $trad[] = "Number of calculation holes";
  $trad[] = "(0 = classic calculation)";
  $trad[] = "Calculation bounds";
  $trad[] = "Page layout";
  $trad[] = "nb of pages :";
  $trad[] = "add correction";
  $trad[] = "Printing method";
  $trad[] = "color";
  $trad[] = "black and white";
  $trad[] = "User manual :<br>- click on the polynomial => switch to another polynomial<br>- click on the question => show correction<br>- click again on the question => switch to next question";
  $trad[] = "Number of questions";
  $trad[] = "Polynomial";
  $trad[] = "Use new polynomial every";
  $trad[] = "questions";
  $trad[] = "generate";
  $trad[] = "© A. RENAUDIN 2020 -- impulse by A. FERREIRA DE SOUZA";
  $trad[] = "free software -- source code";
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta name="mobile-web-app-capable" content="yes">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title><?php echo $trad[0]; ?></title>
  <link rel="shortcut icon" href="polynum.png" >
  <link rel="stylesheet" href="creation2.css">
</head>

<body id="body">
  <div>
    <h1 id="titre"><a href="index.php?lang=<?php echo $lang; ?>"><img src="polynum2.svg" alt="polynum"/></a></h1>
    <div id="expl"><?php echo $trad[1]; ?></div>
    <h2 id="ss-titre">
      <?php
        if ($dest == "pdf.php") echo "<img src='printer.svg' /><span>".$trad[3]."</span>";
        else if ($dest == "video.php") echo "<img src='camera-video.svg' /><span>".$trad[2]."</span>";
        else if ($dest == "jeu.php") echo "<img src='input-gaming.svg' /><span>".$trad[4]."</span>";
      ?>
    </h2>
    <form id="forme" method="GET" action="<?php echo $dest; ?>">
      <input type="hidden" name="lang" value="<?php echo $lang; ?>"/>
      <table>
        <tr>
          <td><?php echo $trad[5]; ?></td>
          <td><input type="number" value="<?php echo $_nbs; ?>" min="3" max="5" length="3" name="nb_sommets" /></td>
        </tr>
        <tr>
          <td><?php echo $trad[6]; ?></td>
          <td>
            <label>min :
              <input type="number" value="<?php echo $_vmin; ?>" name="val_min" />
            </label>
            <label>max :
              <input type="number" value="<?php echo $_vmax; ?>" name="val_max" />
            </label>
          </td>
        </tr>
        <tr>
          <td><?php echo $trad[7]; ?></td>
          <td>
            <label><input type="checkbox" <?php if(array_search("+", $_op)!==false) echo "checked"; ?> name="op_plus" />+</label>
            <label><input type="checkbox" <?php if(array_search("-", $_op)!==false) echo "checked"; ?> name="op_moins" />-</label>
            <label><input type="checkbox" <?php if(array_search("x", $_op)!==false) echo "checked"; ?> name="op_fois" />x</label>
            <label><input type="checkbox" <?php if(array_search(":", $_op)!==false) echo "checked"; ?> name="op_div" />:</label>
          </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <?php
          if ($dest == "pdf.php")
          {
            echo "<tr><td>".$trad[8]."</td>";
            echo "<td><input type='number' value='".$_nbc."' min='1' max='8' name='nb_calc' /> ".$trad[9]."</td></tr>";
          }
        ?>
        <tr>
          <td><?php echo $trad[10]; ?></td>
          <td><input type="number" value="<?php echo $_nbe; ?>" min="1" max="8" name="nb_etapes" /></td>
        </tr>
        <tr>
          <td><?php echo $trad[11]; ?></td>
          <td><input type="number" value="<?php echo $_nbt ?>" min="0" max="8" name="nb_trous" /> <?php echo $trad[12]; ?></td>
        </tr>
        <tr>
          <td><?php echo $trad[13]; ?></td>
          <td>
            <label>min :
              <input type="number" value="<?php echo $_cmin; ?>" name="calc_min" />
            </label>
            <label>max :
              <input type="number" value="<?php echo $_cmax; ?>" name="calc_max" />
            </label>
          </td>
        </tr>
        <?php
          if ($dest == "pdf.php")
          {
            echo "<tr><td>&nbsp;</td></tr>";
            echo "<tr><td>".$trad[14]."</td><td>";
            echo "<label>".$trad[15]."<input type='number' value=".$_nbp." name='nb_pages' min='1' max='3'/></label>";
            echo "<label><input type='checkbox'";
            if($corr) echo " checked";
            echo " name='corr' />".$trad[16]."</label>";
            echo "</td></tr>";
            echo "<tr><td>".$trad[17]."</td><td>";
            echo "<select name='impression'>";
            echo "<option value='couleur' selected>".$trad[18]."</option>";
            echo "<option value='nb'>".$trad[19]."</option>";
            echo "</select>";
            echo "</td></tr>";
          }
          else if ($dest == "video.php")
          {
            echo "<tr><td>&nbsp;</td></tr>";
            echo "<tr><td colspan=2>".$trad[20]."</td></tr>";
          }
          else if ($dest == "jeu.php")
          {
            echo "<tr><td>&nbsp;</td></tr>";
            echo "<tr><td>".$trad[21]."</td><td>";
            echo "<input type='number' value=".$_nbq." name='nb_questions' min='1' max='40'/>";
            echo "</td></tr>";
            echo "<tr><td>".$trad[22]."</td><td>";
            echo "<label>".$trad[23]." <input type='number' value=".$_nbeach." name='nb_change' min='1' max='10'/> ".$trad[24]."</label>";
            echo "</td></tr>";
          }
        ?>

        <tr><td>&nbsp;</td></tr>
        <tr>
          <td colspan="2" id="prefs_gen"><input type="submit" value="<?php echo $trad[25]; ?>" /></td>
        </tr>
      </table>
    </form>
  </div>
  <div id= "space">&nbsp;</div>
  <div id="credits">
    <a href='../contact.php'><img id='contact' src='mail.svg'/><?php echo $trad[26]; ?><img id='contact' src='mail.svg'/></a><br>
    <img src="gpl-v3-logo-nb.svg"/> <a href="https://framagit.org/AlicVB/polynum"><?php echo $trad[27]; ?></a>
    <a href="creation.php?lang=fr&dest=<?php echo $_GET['dest']; ?>"><img src="StampFranceFlag.svg"/></a>
    <a href="creation.php?lang=en&dest=<?php echo $_GET['dest']; ?>"><img src="StampUKFlag.svg"/></a>
  </div>
</body>
</html>
