<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) 2020 A.RENAUDIN  Developer

    This file is part of exoTICE.

    exoTICE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    exoTICE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with exoTICE.  If not, see <https://www.gnu.org/licenses/>.
*/

$rad=36; // rayon des cercles de sommets

function calc($x, $y, $oo)
{
  if ($oo == "+") return $x+$y;
  if ($oo == "-") return $x-$y;
  if ($oo == "x") return $x*$y;
  if ($y != 0 && $oo == ":" && $x % $y == 0) return $x/$y; // pour la division, on accepte que celles entières
  return PHP_INT_MAX; // ca va exclure de-facto l'exercice
}
function op_src($oo)
{
  if ($oo == "+") return "plus.svg";
  if ($oo == "-") return "moins.svg";
  if ($oo == "x") return "fois.svg";
  if ($oo == ":") return "div.svg";
  return "";
}
function op_src_png($oo)
{
  if ($oo == "+") return "plus.png";
  if ($oo == "-") return "moins.png";
  if ($oo == "x") return "fois.png";
  if ($oo == ":") return "div.png";
  return "";
}

function show_ball($xc, $yc, $r, $num, $js="")
{
  $x = $xc - $r;
  $y = $yc - $r;
  $r2 = $r*2;
  $fic = "ball".$num.".svg";
  if ($num < 0) $fic="";
  return "<image class='ballimage' x='".$x."' y='".$y."' width='".$r2."' height='".$r2."' href='".$fic."' ".$js."/>";
}

function show_touche($xc, $yc, $r, $num)
{
  $r2 = $r*2;
  $r3 = $r/2;
  $x = $xc-$r;
  $y = $yc-$r;
  $x1 = $xc - $r*0.5;
  $y1 = $yc - $r*0.5;
  $ret = "<rect class='touche' x='".$x."' y='".$y."' width='".$r2."' height='".$r2."' rx='".$r3."' ry='".$r3."' onclick='touche(".$num.")'/>";
  if ($num <0)
  {
    $ret .= "<image class='itouche' x='".$x1."' y='".$y1."' width='".$r."' height='".$r."' href='moins.svg' onclick='touche(".$num.")'/>";
  }
  elseif ($num > 9)
  {
    $ret .= "<image class='itouche' x='".$x1."' y='".$y1."' width='".$r."' height='".$r."' href='dialog-apply.svg' onclick='touche(".$num.")'/>";
  }
  else
  {
    $ret .= "<text class='ntouche' x='".$xc."' y='".$yc."' text-anchor='middle' dominant-baseline='middle' onclick='touche(".$num.")'>".$num."</text>";
  }
  return $ret;
}

// fonction de traçage du polygone une fois les valeurs définies
// renvoi le code html pour tracer le polynome
// si besoin on joint le tableau des valeurs et opérations (jeu à trou)
function polynum($valeurs, $operations, $vals=false)
{
  global $rad;
  $rad2 = $rad*2;
  $nbs = count($valeurs);

  $ret = "<svg class='fig' height='400px' width='400px' viewbox='0 0 400 400'";
  if ($vals) $ret.=" polyvals='".json_encode(['valeurs'=>$valeurs, 'operations'=>$operations])."'";
  $ret .= ">";
  if ($nbs == 3)
  {
    $yh = $rad + 27;
    $posx = 400-$rad;
    $posy = 373-$rad;
    // les lignes
    $ret .= "<line class='ligne' x1='200' y1='".$yh."' x2='".$rad."' y2='".$posy."'/>";
    $ret .= "<line class='ligne' x1='200' y1='".$yh."' x2='".$posx."' y2='".$posy."'/>";
    $ret .= "<line class='ligne' x1='".$rad."' y1='".$posy."' x2='".$posx."' y2='".$posy."'/>";
    // les cercles avec leur chiffres
    $ret .= show_ball(200, $yh, $rad, 0);
    $ret .= "<text class='nombre' x='200' y='".$yh."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[0]."</text>";
    $ret .= show_ball($rad, $posy, $rad, 1);
    $ret .= "<text class='nombre' x='".$rad."' y='".$posy."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[1]."</text>";
    $ret .= show_ball($posx, $posy, $rad, 2);
    $ret .= "<text class='nombre' x='".$posx."' y='".$posy."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[2]."</text>";
    // les signes opératoires
    $r2 = $rad/2+2;
    $x = 100 + $rad/2;
    $y = 200;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[0][1])."'/>";
    $x = 200;
    $y = 400 - $yh;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[1][2])."'/>";
    $x = 300 - $rad/2;
    $y = 200;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[2][0])."'/>";
  }
  else if ($nbs == 4)
  {
    $posx = 400-$rad;
    $posy = 400-$rad;
    // les lignes
    $ret .= "<line class='ligne' x1='".$rad."' y1='".$rad."' x2='".$rad."' y2='".$posy."'/>";
    $ret .= "<line class='ligne' x1='".$rad."' y1='".$rad."' x2='".$posx."' y2='".$posy."'/>";
    $ret .= "<line class='ligne' x1='".$rad."' y1='".$rad."' x2='".$posx."' y2='".$rad."'/>";
    $ret .= "<line class='ligne' x1='".$posx."' y1='".$rad."' x2='".$rad."' y2='".$posy."'/>";
    $ret .= "<line class='ligne' x1='".$posx."' y1='".$posy."' x2='".$rad."' y2='".$posy."'/>";
    $ret .= "<line class='ligne' x1='".$posx."' y1='".$posy."' x2='".$posx."' y2='".$rad."'/>";
    // les cercles avec leur chiffres
    $ret .= show_ball($rad, $rad, $rad, 0);
    $ret .= "<text class='nombre' x='".$rad."' y='".$rad."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[0]."</text>";
    $ret .= show_ball($posx, $rad, $rad, 1);
    $ret .= "<text class='nombre' x='".$posx."' y='".$rad."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[1]."</text>";
    $ret .= show_ball($posx, $posy, $rad, 2);
    $ret .= "<text class='nombre' x='".$posx."' y='".$posy."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[2]."</text>";
    $ret .= show_ball($rad, $posy, $rad, 3);
    $ret .= "<text class='nombre' x='".$rad."' y='".$posy."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[3]."</text>";
    // les signes opératoires
    $r2 = $rad/2+2;
    $x = 200;
    $y = $rad;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[0][1])."'/>";
    $x = 400 - $rad;
    $y = 200;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[1][2])."'/>";
    $x = 200;
    $y = 400 - $rad;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[2][3])."'/>";
    $x = $rad;
    $y = 200;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[3][0])."'/>";
    $x = 133;
    $y = 133;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[0][2])."'/>";
    $x = 266;
    $y = 133;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[1][3])."'/>";
  }
  else if ($nbs == 5)
  {
    $posx = 400-$rad;
    $posy = 400-$rad;
    $cxy = 200;
    // les lignes
    $ret .= "<line class='ligne' x1='".$rad."' y1='".$rad."' x2='".$rad."' y2='".$posy."'/>";
    $ret .= "<line class='ligne' x1='".$rad."' y1='".$rad."' x2='".$posx."' y2='".$posy."'/>";
    $ret .= "<line class='ligne' x1='".$rad."' y1='".$rad."' x2='".$posx."' y2='".$rad."'/>";
    $ret .= "<line class='ligne' x1='".$posx."' y1='".$rad."' x2='".$rad."' y2='".$posy."'/>";
    $ret .= "<line class='ligne' x1='".$posx."' y1='".$posy."' x2='".$rad."' y2='".$posy."'/>";
    $ret .= "<line class='ligne' x1='".$posx."' y1='".$posy."' x2='".$posx."' y2='".$rad."'/>";
    // les cercles avec leur chiffres
    $ret .= show_ball($rad, $rad, $rad, 0);
    $ret .= "<text class='nombre' x='".$rad."' y='".$rad."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[0]."</text>";
    $ret .= show_ball($posx, $rad, $rad, 1);
    $ret .= "<text class='nombre' x='".$posx."' y='".$rad."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[1]."</text>";
    $ret .= show_ball($posx, $posy, $rad, 2);
    $ret .= "<text class='nombre' x='".$posx."' y='".$posy."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[2]."</text>";
    $ret .= show_ball($rad, $posy, $rad, 3);
    $ret .= "<text class='nombre' x='".$rad."' y='".$posy."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[3]."</text>";
    $ret .= show_ball($cxy, $cxy, $rad, 4);
    $ret .= "<text class='nombre' x='".$cxy."' y='".$cxy."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[4]."</text>";
    // les signes opératoires
    $r2 = $rad/2+2;
    $x = 200;
    $y = $rad;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[0][1])."'/>";
    $x = 400 - $rad;
    $y = 200;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[1][2])."'/>";
    $x = 200;
    $y = 400 - $rad;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[2][3])."'/>";
    $x = $rad;
    $y = 200;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[3][0])."'/>";
    $x = 133;
    $y = 133;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[0][4])."'/>";
    $x = 266;
    $y = 133;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[1][4])."'/>";
    $x = 133;
    $y = 266;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[4][3])."'/>";
    $x = 266;
    $y = 266;
    $x1 = $x - $rad/2;
    $y1 = $y - $rad/2;
    $ret .= "<circle class='oprect' cx='".$x."' cy='".$y."' r='".$r2."' />";
    $ret .= "<image class='opimage' x='".$x1."' y='".$y1."' width='".$rad."' height='".$rad."' href='".op_src($operations[4][2])."'/>";
  }
  $ret .= "</svg>";
  return $ret;
}

// fonction pour poser au hasard les valeurs de sommets et les opérations
// renvoi un tableau avec 1 colonne pour les valeurs des sommets et une colonne pour les opérations
// ['valeurs'] => tableau 1 dimension
// ['operations'] => tableau 2 dimensions avec en x le sommet de départ et en y le sommet d'arrivée
function valeurs($nb_sommets, $val_min, $val_max, $operations)
{
  $ret = [];
  $ret['valeurs'] = [];
  $ret['operations'] = [];

  //on veut du hasard mais aussi éviter les doublons si possible
  $nbs = [];
  for ($i = intval($val_min); $i <= intval($val_max); $i++) $nbs[] = $i;
  for ($i = count($nbs); $i <$nb_sommets; $i++) $nbs[] = random_int(intval($val_min), intval($val_max));
  shuffle($nbs);
  for ($i = 0; $i < $nb_sommets; $i++)
  {
    // le numéro
    $ret['valeurs'][] = $nbs[$i];
    // et les opérations à vide
    $o = [];
    for ($j = 0; $j< $nb_sommets; $j++) $o[] = "";
    $ret['operations'][] = $o;
  }

  // on veut du hasard mais aussi que tous les signes soient représentés
  $nb_op = 3;
  if ($nb_sommets == 4) $nb_op = 6;
  else if ($nb_sommets == 5) $nb_op = 8;
  if ($nb_op < count($operations))
  {
    $ops = $operations;
    shuffle($ops);
    $ops = array_splice($ops, 0, count($ops)-$nb_op);
  }
  else
  {
    $ops = $operations;
    for ($i=0; $i<$nb_op-count($operations); $i++) $ops[] = $operations[random_int(0, count($operations)-1)];
    shuffle($ops);
  }

  $pos_op = 0;
  for ($i = 0; $i < $nb_sommets; $i++)
  {
    for ($j = 0; $j< $nb_sommets; $j++)
    {
      // attention pour 5 sommets il ya des opérations qui n'existent pas (diagonales completes)
      if ($i != $j && $ret['operations'][$i][$j] == "")
      {
        $op_ok = true;
        if ($nb_sommets == 5)
        {
          if ($i == 0 && $j == 2) $op_ok = false;
          else if ($i == 2 && $j == 0) $op_ok = false;
          else if ($i == 1 && $j == 3) $op_ok = false;
          else if ($i == 3 && $j == 1) $op_ok = false;
        }
        if ($op_ok)
        {
          $ret['operations'][$i][$j] = $ops[$pos_op];
          $ret['operations'][$j][$i] = $ret['operations'][$i][$j];
          $pos_op++;
          //if ($pos_op >= count($ops)) $pos_op = 0;
        }
      }
    }
  }

  return $ret;
}

// fonction pour afficher la liste de questions
// renvoi de code html pour afficher la liste des questions
function affiche_questions($questions, $nombre, $nb_trous)
{
  global $rad;

  // on choisit les question au hasard parmis la liste
  $calcs = [];
  if (count($questions)<=$nombre) $calcs = $questions;
  else
  {
    for ($i=0; $i<$nombre; $i++)
    {
      $pos = random_int(0, count($questions)-1);
      $calcs[] = $questions[$pos];
      array_splice($questions, $pos, 1);
    }
  }

  if (count($calcs) == 0) return "";

  $nbe = count($calcs[0])-1;
  $r = min($rad, 0.9*200/count($calcs));
  $w = 2.5*$r*($nbe+1);
  $h = ($r*2 + $r/9)*count($calcs);
  $nb_trous = min($nb_trous, $nbe);

  $ret = "<svg id='calcs' height='".$h."px' width='".$w."px'>";

  $y = $r;
  foreach ($calcs as $ex)
  {
    // la ligne
    $x = 2.5*$r*$nbe;
    $ret .= "<line class='ligne' x1='".$r."' y1='".$y."' x2='".$x."' y2='".$y."'/>";
    $x2 = $x - 0.25*$r;
    $x = $x + 0.25*$r;

    $yy = $y - 0.25*$r;
    $ret .= "<line class='ligne' x1='".$x."' y1='".$y."' x2='".$x2."' y2='".$yy."'/>";
    $yy = $y + 0.25*$r;
    $ret .= "<line class='ligne' x1='".$x."' y1='".$y."' x2='".$x2."' y2='".$yy."'/>";
    // les cercles avec leur chiffres
    $trous = [];
    while (count($trous) < $nb_trous)
    {
      $t = random_int(0, $nbe-1);
      if (array_search($t, $trous)===false) $trous[] = $t;
    }
    for ($i=0; $i<$nbe; $i++)
    {
      $x = $r+2.5*$r*$i;
      if (array_search($i, $trous)!==false)
      {
        $ret .= "<circle class='rond_reponse' cx='".$x."' cy='".$y."' r='".$r."'/>";
      }
      else
      {
        $ret .= show_ball($x, $y, $r, $ex[$i]);
      }
    }

    // et le cercle de réponse
    $x = 1.5*$r+2.5*$r*$nbe;
    $ret .= "<circle class='rond_reponse' cx='".$x."' cy='".$y."' r='".$r."'></circle>";
    $ret .= "<text class='nombre' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'>";
    if (count($trous)>0) $ret .= $ex[$nbe];
    else $ret .= "?";
    $ret .= "</text>";

    $y += $r + $r/9 + $r;
  }

  $ret .= "</svg>";

  return $ret;
}

// fonction pour afficher une seule questions
// renvoi de code html pour afficher la question
function affiche_question($ex, $nb_trous, $corr, $valeurs=null, $operations=null)
{
  global $rad;

  $nbe = count($ex)-1;
  $r = $rad;
  $w = 2.5*$r*($nbe+1);
  $h = $r*2;
  $nb_trous = min($nb_trous, $nbe);

  $ret = "<svg class='calcul' height='".$h."px' width='".$w."px' viewBox='0 0 ".$w." ".$h."'>";

  $y = $r;
  // la ligne
  $x = 2.5*$r*$nbe;
  $ret .= "<line class='ligne' x1='".$r."' y1='".$y."' x2='".$x."' y2='".$y."'/>";
  $x2 = $x - 0.25*$r;
  $x = $x + 0.25*$r;

  $yy = $y - 0.25*$r;
  $ret .= "<line class='ligne' x1='".$x."' y1='".$y."' x2='".$x2."' y2='".$yy."'/>";
  $yy = $y + 0.25*$r;
  $ret .= "<line class='ligne' x1='".$x."' y1='".$y."' x2='".$x2."' y2='".$yy."'/>";
  $cret = $ret;
  // les cercles avec leur chiffres
  $trous = [];
  while (count($trous) < $nb_trous)
  {
    $t = random_int(0, $nbe-1);
    if (array_search($t, $trous)===false) $trous[] = $t;
  }
  for ($i=0; $i<$nbe; $i++)
  {
    $x = $r+2.5*$r*$i;
    if (array_search($i, $trous)!==false)
    {
      $ret .= "<circle class='rond_reponse' cx='".$x."' cy='".$y."' r='".$r."'/>";
      $ret .= "<text class='nombre_reponse' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'>?</text>";

      if ($corr)
      {
        $cret .= show_ball($x, $y, $r, $ex[$i]);
        $cret .= "<circle class='rond_corr' cx='".$x."' cy='".$y."' r='".$r."'/>";
      }

    }
    else
    {
      $ret .= show_ball($x, $y, $r, $ex[$i]);
      $cret .= show_ball($x, $y, $r, $ex[$i]);
    }
  }

  // et le cercle de réponse
  $x = 1.5*$r+2.5*$r*$nbe;
  if (count($trous)>0)
  {
    $ret .= show_ball($x, $y, $r, 9);
    $ret .= "<text class='nombre' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'>".$ex[$nbe]."</text>";
    $cret .= show_ball($x, $y, $r, 9);
    $cret .= "<text class='nombre' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'>".$ex[$nbe]."</text>";
  }
  else
  {
    $ret .= "<circle class='rond_reponse' cx='".$x."' cy='".$y."' r='".$r."'></circle>";
    $ret .= "<text class='nombre_reponse' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'>?</text>";
    $cret .= "<circle class='rond_reponse' cx='".$x."' cy='".$y."' r='".$r."'></circle>";
    $cret .= "<text class='nombre_reponse' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'>".$ex[$nbe]."</text>";
  }

  // si on affiche la correction, il faut aussi afficher le calcul en parallèle
  if ($corr && $valeurs && $operations)
  {
    $y += 2*$r;
    for ($i=0; $i<$nbe; $i++)
    {
      $x = $r+2.5*$r*$i;
      $cret .= "<text class='nombre_reponse' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'>".$valeurs[$ex[$i]]."</text>";
      $x += 1.25*$r;
      if ($i < $nbe-1)
      {
        $cret .= "<text class='nombre_reponse' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'>".$operations[$ex[$i]][$ex[$i+1]]."</text>";
      }
      else $cret .= "<text class='nombre_reponse' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'>=</text>";
    }
    $x = 1.5*$r+2.5*$r*$nbe;
    $cret .= "<text class='nombre_reponse' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'>".$ex[$i]."</text>";
  }

  $ret .= "</svg>";
  $cret .= "</svg>";

  if ($corr) return $ret.$cret;
  else return $ret;
}

// fonction pour afficher une seule questions
// renvoi de code html pour afficher la question
function affiche_question_jeu($tex, $nb_trous, $valeurs)
{
  global $rad;

  $nbe = count($tex[0])-1;
  $r = $rad;
  $w = 2.5*$r*($nbe+1);
  $rres = min($r, $w/(count($tex) + 4));
  $h = $r*2.4 + $r*1.2*4 + $rres + $r;
  $nb_trous = min($nb_trous, $nbe);

  $ret = "<svg class='calculs' height='".$h."px' width='".$w."px' viewBox='0 0 ".$w." ".$h."'>";

  //le listing des exos et le score
  $ret .= "<g>";
  $x = $y = 0;
  for ($i=0; $i<count($tex); $i++)
  {
    $x = $i*$rres;
    $ret .= "<image id='res_ico_".$i."' x='".$x."' y='".$y."' width='".$rres."' height='".$rres."' href='polynum_gris.svg'/>";
  }
  $x = $w;
  $y = $rres/2;
  $ret .= "<text id='res_pourcentage' x='".$x."' y='".$y."' text-anchor='end' dominant-baseline='middle'></text>";
  $ret .= "</g>";

  $y = $r*2 + $rres;
  $trous = [];
  foreach($tex as $ex)
  {
    $ret .= "<g class='calcul'>";
    // la ligne
    $x = 2.5*$r*$nbe;
    $ret .= "<line class='ligne' x1='".$r."' y1='".$y."' x2='".$x."' y2='".$y."'/>";
    $x2 = $x - 0.25*$r;
    $x = $x + 0.25*$r;

    $yy = $y - 0.25*$r;
    $ret .= "<line class='ligne' x1='".$x."' y1='".$y."' x2='".$x2."' y2='".$yy."'/>";
    $yy = $y + 0.25*$r;
    $ret .= "<line class='ligne' x1='".$x."' y1='".$y."' x2='".$x2."' y2='".$yy."'/>";
    // les cercles avec leur chiffres
    $trous = [];
    while (count($trous) < $nb_trous)
    {
      $t = random_int(0, $nbe-1);
      if (array_search($t, $trous)===false) $trous[] = $t;
    }
    for ($i=0; $i<$nbe; $i++)
    {
      $x = $r+2.5*$r*$i;
      if (array_search($i, $trous)!==false)
      {
        $ret .= "<g class='rond trou_".$i."' choix='-1' reponse='".$ex[$i]."'>";
        $ret .= "<circle class='rond_reponse' cx='".$x."' cy='".$y."' r='".$r."'/>";
        $ret .= "<text class='nombre_reponse' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'>?</text>";
        $ret .= show_ball($x, $y, $r*0.8, -1);
        $ret .= "</g>";
      }
      else
      {
        $ret .= "<g class='rond' choix='".$ex[$i]."'>";
        $ret .= show_ball($x, $y, $r, $ex[$i]);
        $ret .= "</g>";
      }
    }

    // et le cercle de réponse
    $x = 1.5*$r+2.5*$r*$nbe;
    if (count($trous)>0)
    {
      $ret .= show_ball($x, $y, $r, 9);
      $ret .= "<text class='nombre nreponse' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle' reponse='".$ex[$nbe]."'>".$ex[$nbe]."</text>";
    }
    else
    {
      $ret .= "<circle class='rond_reponse' cx='".$x."' cy='".$y."' r='".$r."'></circle>";
      $ret .= "<text class='nombre_reponse nreponse' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle' reponse='".$ex[$nbe]."'>?</text>";
    }
    $ret .= "</g>";
  }


  // les éléments pour donner la réponse
  if (count($trous)==0)
  {
    // pavé numérique pour donner la réponse
    $rr = $r*0.6;
    $x = 2.5*$r*($nbe+1) - $rr*5;
    $y = 3*$r + $rres + $r;
    $ret .="<g id='pave_num'>";
    $ret .= show_touche($x, $y, $rr, 7);
    $ret .= show_touche($x+2*$rr, $y, $rr, 8);
    $ret .= show_touche($x+4*$rr, $y, $rr, 9);
    $ret .= show_touche($x, $y+2*$rr, $rr, 4);
    $ret .= show_touche($x+2*$rr, $y+2*$rr, $rr, 5);
    $ret .= show_touche($x+4*$rr, $y+2*$rr, $rr, 6);
    $ret .= show_touche($x, $y+4*$rr, $rr, 1);
    $ret .= show_touche($x+2*$rr, $y+4*$rr, $rr, 2);
    $ret .= show_touche($x+4*$rr, $y+4*$rr, $rr, 3);
    $ret .= show_touche($x, $y+6*$rr, $rr, -1);
    $ret .= show_touche($x+2*$rr, $y+6*$rr, $rr, 0);
    $ret .= show_touche($x+4*$rr, $y+6*$rr, $rr, 10);
    $ret .="</g>";
  }
  else
  {
    // choix de balles sous chaque trou
    $rr = $r*0.4;
    $ret .="<g id='pave_num'>";
    for ($i=0; $i<$nbe; $i++)
    {
      $x = $r+2.5*$r*$i;
      $ret .="<g id='pave_num_".$i."'>";
      for ($j=0; $j<count($valeurs); $j++)
      {
        $y = 3.5*$r + $rres + $rr + $j*($rr*2.2);
        $ret .= show_ball($x, $y, $rr, $j, "onclick='ball_click(".$i.",".$j.")'");
      }
      $ret .= "</g>";
    }
    $ret .="</g>";
  }

  // le groupe pour afficher la correction si c'est faux
  if (count($trous)==0)
  {
    $ret .="<g id='corrige'>";
    // la réponse numérique
    $x = 1.5*$r+2.5*$r*$nbe;
    $y = 3*$r + $rres + $r;
    $ret .= "<g id='corrige_tot'>";
    $ret .= "<text id='corrige_tot_val' x='".$x."' y='".$y."' text-anchor='middle' dominant-baseline='middle'></text>";
    // et la croix
    $x1 = 0.5*$r+2.5*$r*$nbe;
    $x2 = 2.5*$r+2.5*$r*$nbe;
    $y1 = $rres + $r;
    $y2 = $r * 2 + $rres + $r;
    $ret .= "<line class='ligne_corr' x1='".$x1."' y1='".$y1."' x2='".$x2."' y2='".$y2."'/>";
    $ret .= "<line class='ligne_corr' x1='".$x2."' y1='".$y1."' x2='".$x1."' y2='".$y2."'/>";
    $ret .= "</g>";
    $ret .= "</g>";
  }
  else
  {
    $ret .="<g id='corrige'>";
    $y = 2.7*$r + $rres + $r;
    $rr = $r*0.6;
    for ($i=0; $i<$nbe; $i++)
    {
      $ret .= "<g id='corrige_".$i."'>";
      $x = $r+2.5*$r*$i;
      // la balle de réponse
      $ret .= show_ball($x, $y, $rr, $ex[$i]);
      // et la croix
      $x1 = $x-$r;
      $x2 = $x+$r;
      $y1 = $rres + $r;
      $y2 = $r * 2 + $rres + $r;
      $ret .= "<line class='ligne_corr' x1='".$x1."' y1='".$y1."' x2='".$x2."' y2='".$y2."'/>";
      $ret .= "<line class='ligne_corr' x1='".$x2."' y1='".$y1."' x2='".$x1."' y2='".$y2."'/>";
      $ret .= "</g>";
    }
    $ret .= "</g>";
  }

  // le groupe pour afficher le résultat
  $ret .="<g id='final'>";
  // l'icone (a définir dans le js)
  $x = 2.5*$r*($nbe+1) - 5.2*$r;
  $y = 3.4*$r + $rres + $r;
  $rr = 2.8*$r;
  $ret .= "<image id='icone' x='".$x."' y='".$y."' width='".$rr."' height='".$rr."' href='games-highscores.svg'/>";
  // et la flèche suivant
  $x = 2.5*$r*($nbe+1) - 2*$r;
  $y = 4.6*$r + $rres + $r;
  $rr = 2*$r;
  $ret .= "<image id='icone' x='".$x."' y='".$y."' width='".$rr."' height='".$rr."' href='go-next.svg' onclick='suivant()'/>";
  $ret .="</g>";

  // le groupe pour afficher le résultat final
  $ret .= "<g id='finfin'>";
  $x = $r*1.5;
  $y = $rres + $r;
  $ww = $w-3*$r;
  $hh = 6*$r;
  $rm= $r/2;
  $ret .= "<rect id='fin_fond' x='".$x."' y='".$y."' width='".$ww."' height='".$hh."' rx='".$r."' ry='".$r."' onclick='retour()'/>";
  $rr = 5*$r;
  $x = $r*2;
  $y = $y + $r/2;
  $ret .= "<image id='fin_icone' x='".$x."' y='".$y."' width='".$rr."' height='".$rr."' href='games-highscores.svg' onclick='retour()'/>";
  $x = $w-2*$r;
  $y = $rres + $r + 3*$r;
  $ret .= "<text id='fin_tot' x='".$x."' y='".$y."' text-anchor='end' dominant-baseline='middle' onclick='retour()'>100%</text>";
  $ret .= "</g>";

  $ret .= "</svg>";

  return $ret;
}

// fonction pour créer une liste de questions
// renvoi un tableau 2 dimensions avec pour chaque ligne : [numéro_sommet, numero_sommet, numero_sommet, ..., résultat final]
function questions($valeurs, $operations, $nb_etapes, $calc_min, $calc_max)
{
  $nbs = count($valeurs);

  // on crée la liste de toutes les questions possibles a condition qu'elles restent dans les bornes
  for ($i = 0; $i< $nbs; $i++)
  {
    $t1 = $valeurs[$i];
    // deuxième chiffre
    for ($j = 0; $j< $nbs; $j++)
    {
      if ($j == $i) continue;
      $t2 = calc($t1, $valeurs[$j], $operations[$i][$j]);
      if ($t2 < $calc_min || $t2 > $calc_max) continue;
      //troisième chiffre
      if ($nb_etapes > 2)
      {
        for ($k = 0; $k< $nbs; $k++)
        {
          if ($k == $j) continue;
          $t3 = calc($t2, $valeurs[$k], $operations[$j][$k]);
          if ($t3 < $calc_min || $t3 > $calc_max) continue;
          // quatrième chiffre
          if ($nb_etapes > 3)
          {
            for ($l = 0; $l< $nbs; $l++)
            {
              if ($l == $k) continue;
              $t4 = calc($t3, $valeurs[$l], $operations[$k][$l]);
              if ($t4 < $calc_min || $t4 > $calc_max) continue;
              // cinquième chiffre
              if ($nb_etapes > 4)
              {
                for ($m = 0; $m< $nbs; $m++)
                {
                  if ($m == $l) continue;
                  $t5 = calc($t4, $valeurs[$m], $operations[$l][$m]);
                  if ($t5 < $calc_min || $t5 > $calc_max) continue;

                  $calc[] = [$i, $j, $k, $l, $m, $t5];
                }
              }
              else
              {
                $calc[] = [$i, $j, $k, $l, $t4];
              }
            }
          }
          else
          {
            $calc[] = [$i, $j, $k, $t3];
          }
        }
      }
      else
      {
        $calc[] = [$i, $j, $t2];
      }
    }
  }

  return $calc;
}
?>