let pos = 0;
let ppos = 0;
let nbeach = 4;
let reussite = 0;
let fait = 0;

let reponse = null;
let polyvals;

let lang="fr";

// function de calcul
function calcule(v1, v2, op)
{
  if (op == "+") return v1+v2;
  if (op == "-") return v1-v2;
  if (op == "x") return v1*v2;
  if (v2 != 0 && op == ":" && v1 % v2 == 0) return v1/v2; // pour la division, on accepte que celles entières
  return null; // ca va exclure de-facto l'exercice
}

function get_score_coul(score)
{
  if (score < 25) return "#000000";
  else if (score < 50) return "#ff0000";
  else if (score < 75) return "#0000ff";
  else if (score < 100) return "#00cc00";
  return "#fffb00";
}
function get_score_flag(score)
{
  if (score < 25) return "flag-black.svg";
  else if (score < 50) return "flag-red.svg";
  else if (score < 75) return "flag-blue.svg";
  else if (score < 100) return "flag-green.svg";
  return "games-highscores.svg";
}

function retour()
{
  window.location = "creation.php?dest=jeu&lang="+lang;
}

// on affiche les choix de balles en dessous des trous
function setup_trous_choix()
{
  let calc = document.getElementsByClassName("calcul")[pos];
  let els = calc.getElementsByClassName('rond');
  for (let i=0; i<els.length; i++)
  {
    let col = document.getElementById('pave_num_'+i);
    if (col)
    {
      if (els[i].hasAttribute('choix') && parseInt(els[i].getAttribute('choix')) < 0) col.style.display = "block";
      else col.style.display = "none";
    }
  }
}

// pour les questions à trous, on joint au polynome le tableau des valeurs et opérations
// il le faut car on doit recalculer le résultat pour le valider (solutions multiples)
function trous_extrait_poly()
{
  let el = document.getElementsByClassName("fig")[ppos];
  if (el.hasAttribute('polyvals'))
  {
    polyvals = JSON.parse(el.getAttribute('polyvals'));
  }
}

// on passe à la question suivante et on change éventuellement de polynome
function suivant()
{
  // on change le calcul en cours
  let els = document.getElementsByClassName("calcul");
  els[pos].style.display = "none";
  pos++;
  if (pos>=els.length)
  {
    final();
    return;
  }
  els[pos].style.display = "block";

  // on change de polynome si besoin (tout les 4 calculs)
  let pp = Math.floor(pos/nbeach);
  if (pp != ppos)
  {
    els = document.getElementsByClassName("fig");
    if (pp>0) els[pp-1].style.display = "none";
    els[pp].style.display = "block";
    ppos = pp;
    trous_extrait_poly();
  }

  // on masque les résultats
  document.getElementById('corrige').style.display = 'none';
  document.getElementById('final').style.display = 'none';

  // on affiche le pavé numérique
  document.getElementById('pave_num').style.display = 'block';
  // et/ou les colonnes de balles pour les exos à trous
  setup_trous_choix();
}

function depart(_nbeach, _lang)
{
  nbeach = _nbeach;
  lang = _lang;
  let els = document.getElementsByClassName("calcul");
  els[0].style.display = "block";
  els = document.getElementsByClassName("fig");
  els[0].style.display = "block";
  // on extrait les tableaux du polynome en cours
  trous_extrait_poly();
  // on affiche les colonnes de balles pour les exos à trous
  setup_trous_choix();
}

function touche(val)
{
  if (val < 0)
  {
    if (reponse == null) reponse = "-";
    else if (reponse == "-") reponse = null;
    else reponse *= -1;
  }
  else if (val > 9)
  {
    if (reponse != null && reponse != "-") valide();
    return;
  }
  else
  {
    if (reponse == null) reponse = val;
    else if (reponse == "-") reponse = val * -1;
    else if (reponse<0) reponse = reponse * 10 - val;
    else reponse = reponse * 10 + val;
  }

  let el = document.getElementsByClassName("calcul")[pos].getElementsByClassName('nreponse')[0];
  if (reponse != null) el.innerHTML = reponse;
  else el.innerHTML = '';
}

// clic sur une des balles pour remplir un trou
// i=position du trou ; j=balle cliquée
function ball_click(i,j)
{
  let calc = document.getElementsByClassName("calcul")[pos];
  let trou = calc.getElementsByClassName("trou_"+i)[0];
  if (trou)
  {
    let ic = trou.getElementsByClassName("ballimage")[0];
    ic.setAttribute('href', 'ball'+j+'.svg');
    trou.setAttribute('choix', j);
  }
  // si tous les trous sont remplis, on vérifie
  let els = calc.getElementsByClassName('rond');
  let ok = true;
  for (let i=0; i<els.length; i++)
  {
    if (els[i].hasAttribute('choix') && parseInt(els[i].getAttribute('choix')) < 0)
    {
      ok = false;
      break;
    }
  }
  if (ok) trous_valide();
}

function valide()
{
  let e = document.getElementsByClassName("calcul")[pos].getElementsByClassName('nreponse')[0];
  document.getElementById('pave_num').style.display = 'none';
  if (e.textContent == e.getAttribute('reponse'))
  {
    // gagné
    document.getElementById('final').style.display = 'block';
    document.getElementById('icone').setAttribute('href', 'games-highscores.svg');
    document.getElementById('res_ico_'+pos).setAttribute('href', 'games-highscores.svg');
    reussite++;
  }
  else
  {
    // perdu
    document.getElementById('corrige_tot_val').textContent = e.getAttribute('reponse');
    document.getElementById('corrige_tot').style.display = 'block';
    document.getElementById('corrige').style.display = 'block';
    document.getElementById('icone').setAttribute('href', 'flag-red.svg');
    document.getElementById('final').style.display = 'block';

    document.getElementById('res_ico_'+pos).setAttribute('href', 'flag-red.svg');
  }
  fait++;
  reponse = null;

  //et on met à jour le pourcentage
  let p = Math.floor(reussite*100/fait);
  document.getElementById('res_pourcentage').textContent = p + "%";
}

function trous_valide()
{
  let calc = document.getElementsByClassName("calcul")[pos];
  let rep = calc.getElementsByClassName('nreponse')[0].getAttribute('reponse');

  let els = calc.getElementsByClassName('rond');
  let v1, v2;
  let tot = 0;
  for (let i=0; i<els.length; i++)
  {
    v2 = parseInt(els[i].getAttribute('choix'));
    if (i==0) tot = polyvals.valeurs[v2];
    else
    {
      let op = polyvals.operations[v1][v2];
      tot = calcule(tot, polyvals.valeurs[v2], op);
    }
    if (tot == null)
    {
      tot = rep - 1; //comme ça on est sûr que c'est faux !
      break;
    }
    v1=v2;
  }

  document.getElementById('pave_num').style.display = 'none';
  if (tot == rep)
  {
    // gagné
    document.getElementById('final').style.display = 'block';
    document.getElementById('icone').setAttribute('href', 'games-highscores.svg');
    document.getElementById('res_ico_'+pos).setAttribute('href', 'games-highscores.svg');
    reussite++;
  }
  else
  {
    // perdu
    // on affiche la correction pour les trous faux
    for (let i=0; i<els.length; i++)
    {
      if(els[i].hasAttribute('reponse') && parseInt(els[i].getAttribute('choix')) != parseInt(els[i].getAttribute('reponse')))
      {
        let cc = document.getElementById('corrige_'+i);
        cc.getElementsByClassName('ballimage')[0].setAttribute('href', "ball"+parseInt(els[i].getAttribute('reponse'))+".svg");
        cc.style.display = 'block';
      }
      else
      {
        document.getElementById('corrige_'+i).style.display = 'none';
      }
    }
    document.getElementById('corrige').style.display = 'block';
    document.getElementById('icone').setAttribute('href', 'flag-red.svg');
    document.getElementById('final').style.display = 'block';

    document.getElementById('res_ico_'+pos).setAttribute('href', 'flag-red.svg');
  }
  fait++;
  reponse = null;

  //et on met à jour le pourcentage
  let p = Math.floor(reussite*100/fait);
  document.getElementById('res_pourcentage').textContent = p + "%";
}

function final()
{
  //on cache presque tout
  document.getElementById('pave_num').style.display = 'none';
  document.getElementById('final').style.display = 'none';
  document.getElementById('corrige').style.display = 'none';

  //on affiche le final
  let p = Math.floor(reussite*100/fait);
  document.getElementById('fin_tot').textContent = p + "%";
  document.getElementById('fin_tot').style.fill = get_score_coul(p);
  document.getElementById('fin_icone').setAttribute('href', get_score_flag(p));
  document.getElementById('finfin').style.display = 'block';
}