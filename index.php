<?php
 /*
    --En-tête officielle pour dire que ce code est sous une licence "libre" (plus d'infos: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU)--

    Copyright (C) 2020 A.RENAUDIN  Developer

    This file is part of polynum.

    exoTICE is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    exoTICE is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with polynum.  If not, see <https://www.gnu.org/licenses/>.
*/

$lang = "fr";
if ($_GET['lang'] == "en") $lang = "en";

if ($lang == "fr")
{
  $trad[] = "rituel de calcul";
  $trad[] = "pour vidéoprojecteur";
  $trad[] = "pour impression";
  $trad[] = "jeu en ligne";
  $trad[] = "© A. RENAUDIN 2020 -- impulsé par A. FERREIRA DE SOUZA";
  $trad[] = "logiciel libre -- code source";
}
else if ($lang == "en")
{
  $trad[] = "mental calculation";
  $trad[] = "for projectors";
  $trad[] = "for printers";
  $trad[] = "online game";
  $trad[] = "© A. RENAUDIN 2020 -- impulse by A. FERREIRA DE SOUZA";
  $trad[] = "free software -- source code";
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta name="mobile-web-app-capable" content="yes">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>polynum</title>
  <link rel="shortcut icon" href="polynum.png" >
  <link rel="stylesheet" href="creation2.css">
</head>

<body id="body">
  <div>
    <h1 id="titre"><img src="polynum2.svg" alt="polynum"/></h1>
    <div id="expl"><?php echo $trad[0]; ?></div>
    <h2>
        <a href="creation.php?dest=video&lang=<?php echo $lang; ?>">
            <img src="camera-video.svg" />
            <span><?php echo $trad[1]; ?></span>
        </a>
    </h2>
    <h2>
        <a href="creation.php?dest=pdf&lang=<?php echo $lang; ?>">
            <img src="printer.svg" />
            <span><?php echo $trad[2]; ?></span>
        </a>
    </h2>
    <h2>
        <a href="creation.php?dest=jeu&lang=<?php echo $lang; ?>">
            <img src="input-gaming.svg" />
            <span><?php echo $trad[3]; ?></span>
        </a>
    </h2>
  </div>
  <div id= "space">&nbsp;</div>
  <div id="credits">
  <a href='../contact.php'><img id='contact' src='mail.svg'/><?php echo $trad[4]; ?><img id='contact' src='mail.svg'/></a><br>
    <img src="gpl-v3-logo-nb.svg"/> <a href="https://framagit.org/AlicVB/polynum"><?php echo $trad[5]; ?></a>
    <a href="index.php?lang=fr"><img src="StampFranceFlag.svg"/></a>
    <a href="index.php?lang=en"><img src="StampUKFlag.svg"/></a>
  </div>
</body>
</html>
